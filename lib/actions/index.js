/*
    Action for artist model
 */

import ActionTypes from "../constants/ActionsTypes";

export const pushArtist = artist => {
    return {
        type: ActionTypes.PUSH_ARTIST,
        payload: artist
    };
};