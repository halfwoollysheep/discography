import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import Artist from './components/Artist/Artist';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'

import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reduxApp from './reducers'

let store = createStore(reduxApp);

const Root = () => (
    <Provider store={store}>
        <Router>
            <div>
                <Switch>
                    <Route exact path="/" component={App}/>
                    <Route exact path="/artist/:id" component={Artist}/>
                </Switch>
            </div>
        </Router>
    </Provider>
);



ReactDOM.render(<Root />, document.getElementById('root'));
