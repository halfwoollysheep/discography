/*
// @flow
type Action = {
    type: String;
    payload: Object;
};

import ActionTypes from "../constants/ActionsTypes"

const initialState = {
    artist: []
};

const artists = (state: Array<Object> = [], action: Action): Object => {
    switch (action.type) {
        case ActionTypes.PUSH_ARTIST:
            return { artist: action.payload };
        default:
            return { state }
    }
}

export default artists */

/*
    Reducer for Artist
 */

import ActionTypes from "../constants/ActionsTypes"

const initialState = {
    artist: []
};

const artists = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.PUSH_ARTIST:
            return { artist: action.payload };
        default:
            return { state }
    }
}

export default artists