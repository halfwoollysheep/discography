import React from 'react'
import "./Item.css"
import PropTypes from 'prop-types';
import {
    Link
} from 'react-router-dom'

/*
    Tile for artists results
 */
export default class Item extends React.PureComponent {
    constructor(props){
        super(props);
    }

    onItemClicked = () => {
        this.props.onClick(this.props.artist);
    };

    render(){
        const style = {
            backgroundImage: "url(" + this.props.image + ")"
        }
        return (
            <Link to={"/artist/" + this.props.artist.id}
                  className="item"
                  style={style}
                  onClick={this.onItemClicked}>
                <div className="label">{this.props.artist.name}</div>
            </Link>
        )
    }
}

Item.propTypes = {
    image: PropTypes.string,
    artist: PropTypes.object,
    onClick: PropTypes.func,
}

